package main

import (
	"fmt"
	"gitee.com/iris_huahua/go-notice/route"
	"net/http"
)

func main() {
	fmt.Println("access http://localhost:8080/notice")
	route.Route()
	http.ListenAndServe(":8080", nil)
}
