all: push

PRJ_NAME = go-notice
VERSION = v1.0
TAG = $(VERSION)
PREFIX = registry.cn-beijing.aliyuncs.com/nmyx/$(PRJ_NAME)

DOCKER_BUILD_RUN = docker run --rm -v $(shell pwd):/home/server/$(PRJ_NAME) -w /home/server/$(PRJ_NAME)
GOLANG_CONTAINER = golang:1.17.3
DOCKERFILE = Dockerfile

build-src:
	$(DOCKER_BUILD_RUN) $(GOLANG_CONTAINER) go build main.go

build-image: build-src
	docker build -t $(PREFIX):$(TAG) .

push: build-image
	docker push $(PREFIX):$(TAG)

clean:
	rm -f main
