package bean

type WxAlert struct {
	Receiver string `json:"receiver"`
	Status   string `json:"status"`
	Alerts   []struct {
		Status string `json:"status"`
		Labels struct {
			AlertName   string `json:"alertname"`
			Env         string `json:"env"`
			Instance    string `json:"instance"`
			Application string `json:"application"`
			Severity    string `json:"severity"`
			Type        string `json:"type"`
		} `json:"labels"`
		Annotations struct {
			Description string `json:"description"`
			Summary     string `json:"summary"`
		} `json:"annotations"`
		StartsAt     string `json:"startsAt"`
		EndsAt       string `json:"endsAt"`
		GeneratorURL string `json:"generatorURL"`
		Fingerprint  string `json:"fingerprint"`
	} `json:"alerts"`
	GroupLabels struct {
		AlertName string `json:"alertname"`
	} `json:"groupLabels"`
	CommonLabels struct {
		AlertName string `json:"alertname"`
		Env       string `json:"env"`
		Instance  string `json:"instance"`
		Severity  string `json:"severity"`
		Type      string `json:"type"`
	} `json:"commonLabels"`
	CommonAnnotations struct {
		Description string `json:"description"`
		Summary     string `json:"summary"`
	} `json:"commonAnnotations"`
	ExternalURL     string `json:"externalURL"`
	Version         string `json:"version"`
	GroupKey        string `json:"groupKey"`
	TruncatedAlerts int    `json:"truncatedAlerts"`
}
