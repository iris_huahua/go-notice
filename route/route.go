package route

import (
	"gitee.com/iris_huahua/go-notice/api"
	"net/http"
)

func Route() {
	http.HandleFunc("/notice/", api.Index)
	http.HandleFunc("/notice/api/alert/wx", api.AlertWx)
	http.HandleFunc("/notice/api/alert/ali", api.AlertAli)
	http.HandleFunc("/notice/api/common/wx", api.CommonWx)
	http.HandleFunc("/notice/api/common/ali", api.CommonAli)
}
