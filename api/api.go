package api

import (
	"encoding/json"
	"fmt"
	"gitee.com/iris_huahua/go-notice/bean"
	"gitee.com/iris_huahua/go-notice/conf"
	"gitee.com/iris_huahua/go-notice/utils"
	"io/ioutil"
	"net/http"
	"strings"
)

type Param struct {
	Data string `json:"data"`
}

func Index(writer http.ResponseWriter, request *http.Request) {
	_, err := writer.Write([]byte("service is running!!!"))
	if err != nil {
		fmt.Println("has error ", err)
		return
	}
}

//AlertWx alert-manager 企业微信通知
func AlertWx(writer http.ResponseWriter, request *http.Request) {

	if request.Method != "POST" {
		writer.WriteHeader(405)
		writer.Write([]byte("request method error!!!"))
		return
	}

	key := conf.WxKey
	if v, ok := request.URL.Query()["key"]; ok {
		key = v[0]
	}

	param := parseAlertManagerBodyWx(request)

	utils.SendWxMsg(key, param)

	writer.Write([]byte("success !!!"))
}

//CommonWx 企业微信通知-通用
func CommonWx(writer http.ResponseWriter, request *http.Request) {

	if request.Method != "POST" {
		writer.WriteHeader(405)
		writer.Write([]byte("request method error!!!"))
		return
	}

	key := conf.WxKey
	if v, ok := request.URL.Query()["key"]; ok {
		key = v[0]
	}

	request.ParseForm()

	content := request.FormValue("content")

	utils.SendWxMsg(key, content)

	writer.Write([]byte("success !!!"))
}

//parseAlertManagerBodyWx 解析alert-manager发送的消息并最终组装成企业微信的报文
func parseAlertManagerBodyWx(request *http.Request) string {

	err := request.ParseForm()
	if err != nil {
		utils.LogError.Println("has error ", err)
		return ""
	}

	var wxAlert bean.WxAlert
	body, _ := ioutil.ReadAll(request.Body)

	utils.LogInfo.Println(string(body))

	json.Unmarshal(body, &wxAlert)

	var temp = wxAlert.Alerts[0]
	var labels = temp.Labels

	sb := fmt.Sprint("<font size='20'>**PROMETHEUS报警**</font>\n监控指标：", labels.AlertName, "\n服务状态：")
	if temp.Status == "resolved" {
		sb = fmt.Sprint(sb, "<font color='green'>")
	} else {
		sb = fmt.Sprint(sb, "<font color='red'>")
	}
	sb = fmt.Sprint(sb, temp.Status, "</font>\n告警等级：")

	if temp.Status == "resolved" {
		sb = fmt.Sprint(sb, "<font color='green'>")
	} else {
		if labels.Severity == "FATAL" {
			sb = fmt.Sprint(sb, "<font color='red'>")
		} else {
			sb = fmt.Sprint(sb, "<font color='warning'>")
		}
	}
	sb = fmt.Sprint(sb, labels.Severity, "</font>\n")

	if strings.Contains(temp.EndsAt, "0001") {
		sb = fmt.Sprint(sb, "告警时间：", utils.NowYmdHms(), "\n")
	} else {
		sb = fmt.Sprint(sb, "恢复时间：", utils.NowYmdHms(), "\n")
	}

	if labels.Type == "public" {
		sb = fmt.Sprint(sb, "告警服务：")
		for _, alt := range wxAlert.Alerts {
			labels = alt.Labels
			instance := labels.Instance
			if instance == "" {
				instance = labels.Application
			}
			sb = fmt.Sprint(sb, "**", instance, "**、")
		}
		sb = fmt.Sprint(sb, "\n")
		description := temp.Annotations.Description
		sb = fmt.Sprint(sb, "告警描述：", description, "\n")
	} else {
		for _, alt := range wxAlert.Alerts {
			sb = fmt.Sprint(sb, "告警服务：")
			labels = alt.Labels
			instance := labels.Instance
			if instance == "" {
				instance = labels.Application
			}
			sb = fmt.Sprint(sb, "**", instance, "**\n")
			description := temp.Annotations.Description
			sb = fmt.Sprint(sb, "告警描述：", description, "\n")
		}
	}
	return sb
}

//parseAlertManagerBodyAli 解析alert-manager发送的消息并最终组装成钉钉的markdown报文
func parseAlertManagerBodyAli(request *http.Request) string {

	err := request.ParseForm()
	if err != nil {
		utils.LogError.Println("has error ", err)
		return ""
	}

	var wxAlert bean.WxAlert
	body, _ := ioutil.ReadAll(request.Body)

	utils.LogInfo.Println(string(body))

	json.Unmarshal(body, &wxAlert)

	var temp = wxAlert.Alerts[0]
	var labels = temp.Labels

	sb := fmt.Sprint("#### **PROMETHEUS告警**\n###### 监控指标：", labels.AlertName, "\n###### 服务状态：**", temp.Status, "**\n###### 告警等级：", labels.Severity, "\n")

	if strings.Contains(temp.EndsAt, "0001") {
		sb = fmt.Sprint(sb, "###### 告警时间：", utils.NowYmdHms(), "\n")
	} else {
		sb = fmt.Sprint(sb, "###### 恢复时间：", utils.NowYmdHms(), "\n")
	}

	if labels.Type == "public" {
		sb = fmt.Sprint(sb, "###### 告警服务：")
		for _, alt := range wxAlert.Alerts {
			labels = alt.Labels
			instance := labels.Instance
			if instance == "" {
				instance = labels.Application
			}
			sb = fmt.Sprint(sb, instance, "、")
		}
		sb = fmt.Sprint(sb, "\n")
		description := temp.Annotations.Description
		sb = fmt.Sprint(sb, "###### 告警描述：", description, "\n")
	} else {
		for _, alt := range wxAlert.Alerts {
			sb = fmt.Sprint(sb, "###### 告警服务：")
			labels = alt.Labels
			instance := labels.Instance
			if instance == "" {
				instance = labels.Application
			}
			sb = fmt.Sprint(sb, instance, "\n")
			description := temp.Annotations.Description
			sb = fmt.Sprint(sb, "###### 告警描述：", description, "\n")
		}
	}
	return sb
}

//AlertAli alert-manager 阿里钉钉通知
func AlertAli(writer http.ResponseWriter, request *http.Request) {
	if request.Method != "POST" {
		writer.WriteHeader(405)
		writer.Write([]byte("request method error!!!"))
		return
	}

	token := conf.AliDingTalkAccessToken
	if v, ok := request.URL.Query()["token"]; ok {
		token = v[0]
	}

	param := parseAlertManagerBodyAli(request)

	utils.SendAliMsgMD(token, param)

	writer.Write([]byte("send success!!!"))
}

//CommonAli 阿里钉钉通用通知
func CommonAli(writer http.ResponseWriter, request *http.Request) {
	if request.Method != "POST" {
		writer.WriteHeader(405)
		writer.Write([]byte("request method error!!!"))
		return
	}

	token := conf.AliDingTalkAccessToken
	if v, ok := request.URL.Query()["token"]; ok {
		token = v[0]
	}

	request.ParseForm()

	content := request.FormValue("content")

	utils.SendAliMsg(token, content)

	writer.Write([]byte("send success!!!"))
}
