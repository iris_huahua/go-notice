package utils

import (
	"fmt"
	"gitee.com/iris_huahua/go-notice/conf"
	"gitee.com/iris_huahua/go-notice/req"
)

func SendAliMsg(token string, content string) {
	msg := `{"msgtype":"text","text":{"content":"通知：` + content + `"},"at":{"isAtAll":true}}`
	fmt.Println(msg)
	resp := req.DoPostJson(conf.AliDingTalkUrl+token, msg)
	LogInfo.Println("SendAliMsg resp: ", resp)
}

func SendAliMsgMD(token string, content string) {
	msg := `{"msgtype":"markdown","markdown":{"title":"监控告警", "text":"` + content + `"},"at":{"isAtAll":true}}` //注意这个isAtAll是true不是带双引号的"true"
	fmt.Println(msg)
	resp := req.DoPostJson(conf.AliDingTalkUrl+token, msg)
	LogInfo.Println("SendAliMsg resp: ", resp)
}

func SendWxMsg(key string, content string) {
	msg := `{"msgtype":"markdown","markdown":{"content":"` + content + `","mentioned_mobile_list":["@all"]}}`
	resp := req.DoPostJson(conf.WxUrl+key, msg)

	LogInfo.Println(resp)
}
