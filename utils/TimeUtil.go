package utils

import (
	"time"
)

func NowYmd() string {
	now := time.Now()
	return now.Format("2006-01-02")
}

func NowYmdHms() string {
	now := time.Now()
	return now.Format("2006-01-02 15:04:05")
}

//ToYmd Millisecond
func ToYmd(val int64) string {
	now := time.Unix(val/1e3, 0)
	return now.Format("2006-01-02")
}

//ToYmdHms Millisecond
func ToYmdHms(val int64) string {
	now := time.Unix(val/1e3, 0)
	return now.Format("2006-01-02 15:04:05")
}
