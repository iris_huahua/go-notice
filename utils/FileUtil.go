package utils

import (
	"fmt"
	"os"
)

//FileIsExist 判断文件或文件夹是否存在
func FileIsExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		if os.IsNotExist(err) {
			return false
		}
		return false
	}
	return true
}

//MkDriLoop 递归创建文件夹
func MkDriLoop(path string) bool {
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}
