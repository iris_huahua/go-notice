package utils

import (
	"io"
	"io/ioutil"
	"log"
	"os"
)

var (
	LogTrace   *log.Logger // 记录所有日志
	LogInfo    *log.Logger // 重要的信息
	LogWarning *log.Logger // 需要注意的信息
	LogError   *log.Logger // 非常严重的问题
)

func init() {
	MkDriLoop("/log")
	fileErr, err := os.OpenFile("/log/error.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalln("Failed to open error log fileErr:", err)
	}
	fileInfo, err2 := os.OpenFile("/log/info.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err2 != nil {
		log.Fatalln("Failed to open info log fileInfo:", err2)
	}

	LogTrace = log.New(ioutil.Discard, "TRACE: ", log.Ldate|log.Ltime|log.Lshortfile)

	LogInfo = log.New(io.MultiWriter(fileInfo, os.Stdout), "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)

	LogWarning = log.New(os.Stdout, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)

	LogError = log.New(io.MultiWriter(fileErr, os.Stderr), "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}
