package req

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func DoGet(url string) HttpResponse {
	resp, err := http.Get(url)

	if err != nil {
		fmt.Println("get failed, err:", err)
		return HttpResponse{
			Code:  resp.StatusCode,
			Value: "get failed, err: " + err.Error(),
		}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("read from resp.Body failed,err:", err)
		return HttpResponse{
			Code:  resp.StatusCode,
			Value: "read from resp.Body failed,err:" + err.Error(),
		}
	}
	return HttpResponse{
		Code:  resp.StatusCode,
		Value: string(body),
	}
}

func DoPost(url string, data string) HttpResponse {
	contentType := "application/x-www-form-urlencoded"
	//data := "name=枯藤&age=18"
	resp, err := http.Post(url, contentType, strings.NewReader(data))
	if err != nil {
		return HttpResponse{
			Code:  resp.StatusCode,
			Value: "post failed, err:" + err.Error(),
		}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return HttpResponse{
			Code:  resp.StatusCode,
			Value: "get resp failed,err:" + err.Error(),
		}
	}
	return HttpResponse{
		Code:  resp.StatusCode,
		Value: string(body),
	}
}

func DoPostJson(url string, json string) HttpResponse {
	contentType := "application/json"
	resp, err := http.Post(url, contentType, strings.NewReader(json))
	if err != nil {
		return HttpResponse{
			Code:  resp.StatusCode,
			Value: "post failed, err:" + err.Error(),
		}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return HttpResponse{
			Code:  resp.StatusCode,
			Value: "get resp failed,err:" + err.Error(),
		}
	}
	return HttpResponse{
		Code:  resp.StatusCode,
		Value: string(body),
	}
}

func main() {

	//result := DoGet("https://www.jianshu.com/p/58dcbf490ef3")
	//fmt.Println(result)

	//result := DoPost("https://www.jianshu.com/p/58dcbf490ef3", "name=89")
	//fmt.Println(result)

	result := DoPostJson("https://www.jianshu.com/p/58dcbf490ef3", `{"name":"枯藤","age":18}`)
	fmt.Println(result)
}
