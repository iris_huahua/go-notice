package req

type HttpResponse struct {
	Code  int
	Value string
}
