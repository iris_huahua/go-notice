FROM registry.cn-beijing.aliyuncs.com/nmyx/alpine-glibc:v1.0.time
ADD main /home/server/
WORKDIR /home/server
EXPOSE 8080
ENTRYPOINT ["/home/server/main"]